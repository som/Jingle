#!/usr/bin/gjs

/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/* exported main */

imports.gi.versions.Gtk = '4.0';
const { Gio, GLib, GObject, Gtk } = imports.gi;

const BuilderScope = GObject.registerClass({
    Implements: [Gtk.BuilderScope],
}, class extends GObject.Object {
    // Extend the default typename resolution.
    vfunc_get_type_from_name(builder, typeName) {
        if (!GObject.type_from_name(typeName))
            Gtk.test_register_all_types();

        if (!GObject.type_from_name(typeName)) {
            if (typeName.startsWith('G')) {
                void Gio[typeName.slice(1)];
                void GLib[typeName.slice(1)];
                void GObject[typeName.slice(1)];
            } else if (typeName.startsWith('Adw')) {
                try {
                    void imports.gi.Adw[typeName.slice(3)];
                } catch(e) {}
            }
        }

        return GObject.type_from_name(typeName) ?? `Unknown type "${typeName}"`;
    }

    // Trap the handlers.
    vfunc_create_closure(builder, handlerName, flags, object) {
        return new Function();
    }
});

const registerJSClasses = async function(files) {
    let classes = [];
    let registerClass = GObject.registerClass;
    GObject.registerClass = function(...args) {
        let Klass = registerClass(...args);
        classes.push(Klass);
        return Klass;
    };

    await Promise.all(files.map(file => import(file.get_uri())));

    GObject.registerClass = registerClass;
    return classes;
};

let application = new Gtk.Application({
    flags: Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
});

application.add_main_option(
    'id', 'i'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.STRING,
    "Specify the ID to pick in builder files", null
);
application.add_main_option(
    'no-header-bar', 'n'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.NONE,
    "Initialize the window without a header bar", null
);
application.add_main_option(
    'type', 't'.charCodeAt(0), GLib.OptionFlags.NONE, GLib.OptionArg.STRING,
    "Specify a type name for the toplevel widget", null
);
application.set_option_context_parameter_string("[FILE…]");
application.set_option_context_description(
    `The command accepts builder files to start the application with, otherwise the window will be an empty playground.
The command accepts JavaScript files to register custom GObject classes, to be consumed by the builder files as well as the in-app packing feature.`
);

application.connect('command-line', (application, cmdLine) => {
    application._topId = cmdLine.get_options_dict().lookup_value('id', null)?.unpack();
    application._noHeaderBar = !!cmdLine.get_options_dict().lookup_value('no-header-bar', null);
    application._typeName = cmdLine.get_options_dict().lookup_value('type', null)?.unpack();

    let jsFiles = [], uiFiles = [], [, ...files] = cmdLine.get_arguments().map(arg => cmdLine.create_file_for_arg(arg));
    files.forEach(file => {
        try {
            let info = file.query_info(Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE, Gio.FileQueryInfoFlags.NONE, null);
            if (info.get_content_type() == 'application/javascript')
                jsFiles.push(file);
            else if (info.get_content_type() == 'application/x-gtk-builder')
                uiFiles.push(file);
            else
                throw new Error(`${file.get_basename()} is not supported`);
        } catch(e) {
            log(e.message);
        }
    });

    application._uiFiles = uiFiles;

    if (jsFiles.length) {
        application.hold();

        registerJSClasses(jsFiles).then(classes => {
            application._classes = classes;
            application.activate();
        }).catch(logError).finally(() => {
            application.release();
        });
    } else {
        application.activate();
    }
});

application.connect('activate', application => {
    let topWidget, builder;

    if (application._uiFiles.length) {
        builder = new Gtk.Builder({ scope: new BuilderScope() });
        application._uiFiles.forEach(file => {
            try {
                builder.add_from_file(file.get_path());
            } catch(e) {
                log(e.message);
            }
        });

        topWidget = builder.get_object(application._topId ?? 'jingle-widget');

        if (topWidget?.parent)
            log(`${topWidget.get_buildable_id()} already has a parent`);

        if (!topWidget || topWidget.parent) {
            let topWidgets = builder.get_objects().filter(object => object instanceof Gtk.Widget).map(widget => {
                let parent = widget;
                while (parent.get_parent())
                    parent = parent.get_parent();
                return parent;
            });
            let windows = topWidgets.filter(widget => widget instanceof Gtk.Window);

            topWidget = (
                windows.find(window => window instanceof Gtk.ApplicationWindow) ??
                windows.find(window => !(window instanceof Gtk.AboutDialog) && !(window instanceof Gtk.Dialog) && !(window instanceof Gtk.Assistant) && !(window instanceof Gtk.ShortcutsWindow)) ??
                topWidgets.find(widget => !(widget instanceof Gtk.Window)) ??
                windows[0] ??
                null
            );
        }
    }

    if (!topWidget && application._typeName) {
        let constructor, typeName = application._typeName;
        if (typeName.startsWith('Gtk')) {
            try {
                if (Gtk[typeName.slice(3)]?.$gtype?.name == typeName)
                    constructor = Gtk[typeName.slice(3)];
            } catch(e) {}
        } else if (typeName.startsWith('Adw')) {
            try {
                if (imports.gi.Adw[typeName.slice(3)]?.$gtype?.name == typeName)
                    constructor = imports.gi.Adw[typeName.slice(3)];
            } catch(e) {}
        }

        if (!constructor && application._classes)
            constructor = application._classes.find(Klass => Klass.$gtype.name == typeName);

        if (!constructor)
            log(`Unknown type "${typeName}"`);
        else if (constructor.$gtype && GObject.type_is_a(constructor.$gtype, Gtk.Widget.$gtype))
            topWidget = new constructor();
        else
            log(`"${typeName}" is not a widget type`);
    }

    let placeholder, window = topWidget instanceof Gtk.Window ? topWidget : new Gtk.Window({
        title: "Hello World",
        defaultWidth: 600, defaultHeight: 300,
        child: topWidget ?? (placeholder = new Gtk.Button({
            halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
            child: new Gtk.ShortcutsShortcut({
                title: "Open the inspector",
                accelerator: '<ctrl>d <ctrl>i',
            }),
        })),
    });

    window.set_application(application);

    if ((window != topWidget) && !application._noHeaderBar)
        window.set_titlebar(new Gtk.HeaderBar());

    window.present();

    if (placeholder) {
        placeholder.connect('clicked', () => Gtk.Window.set_interactive_debugging(true));

        // Remove the placeholder on first inspector opening.
        let topLevels = Gtk.Window.get_toplevels();
        let topLevelsHandler = topLevels.connect('items-changed', (list, position, removed_, added) => {
            for (let i = 0; i <= added - 1; i++) {
                if (list.get_item(position + i).toString().includes('GtkInspectorWindow')) {
                    window.child = null;
                    topLevels.disconnect(topLevelsHandler);

                    break;
                }
            }
        });
    }

    globalThis.jbuilder = builder ?? null;
    globalThis.jclasses = application._classes ?? null;

    globalThis.jwin = null;
    application.connect('notify::active-window', application => {
        globalThis.jwin = application.activeWindow;
    });

    import('./evaluator/evaluator.js').then(({ default: Evaluator }) => {
        let evaluator = new Evaluator();

        Evaluator.addToInspector(evaluator);
    }).catch(error => {
        // Evaluator is not strongly required.
        log(`The Evaluator module is not available:\n${error.message}`);
    });

    import('./gladiator/gladiator.js').then(({ default: Gladiator }) => {
        let gladiator = new Gladiator({ builder: builder ?? null, classes: application._classes ?? null });

        globalThis.jobj = null;
        gladiator.connect('notify::inspected-object', gladiator => {
            globalThis.jobj = gladiator.inspectedObject;
        });

        Gladiator.addToInspector(gladiator);
    }).catch(logError);
});

function main(args, appId) {
    application.run(args);
}
